package br.edu.up.exercicio3;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void jogar(View view) {

        EditText txtNome = (EditText) findViewById(R.id.txtNome);
        EditText txtCPF = (EditText) findViewById(R.id.txtCPF);
        EditText txtCelular = (EditText) findViewById(R.id.txtCelular);
        EditText txtDataNascimento = (EditText) findViewById(R.id.txtDataNascimento);

        String nome = txtNome.getText().toString();
        String cpf = txtCPF.getText().toString();
        String celular = txtCelular.getText().toString();
        String dataNascimento = txtDataNascimento.getText().toString();

        Jogador jogador = null;
        try {
            jogador = new Jogador(nome, cpf, celular, dataNascimento);
        } catch (CPFInvalidoException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        } catch (MenorDeIdadeException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        } catch (DataInvalidaException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Intent in = new Intent(this, DadosActivity.class);
        in.putExtra("jogador", jogador);
        startActivity(in);
    }
}
