package br.edu.up.exercicio3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class DadosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados);

        Intent in = getIntent();
        Jogador jogador = (Jogador) in.getSerializableExtra("jogador");

        TextView txtNome = (TextView) findViewById(R.id.txtNome);
        txtNome.setText(jogador.getNome());

        TextView txtCPF = (TextView) findViewById(R.id.txtCPF);
        txtCPF.setText(jogador.getCpf());

        TextView txtIdade = (TextView) findViewById(R.id.txtIdade);
        txtIdade.setText(String.valueOf(jogador.getIdade()));

        TextView txtCelular = (TextView) findViewById(R.id.txtCelular);
        txtCelular.setText(jogador.getCelular());

    }
}