package br.edu.up.exercicio3;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 14/10/2013
 */
public class MenorDeIdadeException extends Throwable {

    public MenorDeIdadeException(){
        this(Util.getMensagem(R.string.msg_menor_de_idade));
    }

    public MenorDeIdadeException(String msg){
        super(msg);
    }
}
