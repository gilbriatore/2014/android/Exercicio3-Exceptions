package br.edu.up.exercicio3;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 14/10/2013
 */
public class DataInvalidaException extends Throwable {

    public DataInvalidaException(){
        this(Util.getMensagem(R.string.msg_data_invalida));
    }

    public DataInvalidaException(String msg){
        super(msg);
    }
}
