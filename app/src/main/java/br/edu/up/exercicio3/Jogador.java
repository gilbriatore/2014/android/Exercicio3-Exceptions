package br.edu.up.exercicio3;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 27/08/2015
 */
public class Jogador implements Serializable {


    private final String nome;
    private final String cpf;
    private final String celular;
    private final String dataNascimento;
    private  int idade;

    public Jogador(String nome, String cpf, String celular, String dataNascimento)
            throws CPFInvalidoException, MenorDeIdadeException, DataInvalidaException {
        this.nome = nome;

        if(!Util.isCPFValido(cpf)){
            throw new CPFInvalidoException();
        }

        this.cpf = cpf;
        this.celular = celular;

        if (Util.isMenorDeIdade(dataNascimento)){
            throw new MenorDeIdadeException();
        }
        this.dataNascimento = dataNascimento;

        this.idade = Util.getIdade(dataNascimento);
    }

    public int getIdade(){
        return idade;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getCelular() {
        return celular;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }
}