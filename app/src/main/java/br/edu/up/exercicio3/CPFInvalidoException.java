package br.edu.up.exercicio3;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 14/10/2013
 */
public class CPFInvalidoException extends Throwable {

    public CPFInvalidoException(){
        this(Util.getMensagem(R.string.msg_cpf_invalido));
    }

    public CPFInvalidoException(String msg){
        super(msg);
    }
}
