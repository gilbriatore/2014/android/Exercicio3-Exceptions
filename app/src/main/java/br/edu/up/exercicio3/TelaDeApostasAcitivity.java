package br.edu.up.exercicio3;

import android.app.ActionBar;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

public class TelaDeApostasAcitivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_de_apostas_acitivity);

        final TableLayout layoutPrincipal = (TableLayout) findViewById(R.id.layoutPrincipal);
        TableLayout.LayoutParams params = getLayoutParams();
        params.setMargins(0,0,0,0);
        for (int i = 0; i < 10; i++) {

            final TableRow linha = new TableRow(this);
            linha.setLayoutParams(params);

            for (int j = 1; j <= 6; j++) {
                ToggleButton btn = new ToggleButton(this);
                btn.setPadding(0, 0, 0, 0);
                btn.setWidth(30);
                //TableLayout.LayoutParams p = getLayoutParams();
                //p.weight = 1;
                //btn.setLayoutParams(getLayoutParams());
                //getLayoutParams().weight = 1;
                btn.setText(String.valueOf(i + j));
                btn.setTextOn("X");
                btn.setTextOff(String.valueOf(i + j));
                //Button btn = new Button(this);
                //btn.setText(String.valueOf(j));
                linha.addView(btn);
            }
            layoutPrincipal.addView(linha, params);
        }

       /*

        LinearLayout layoutHorizontal = new LinearLayout(this);
        layoutHorizontal.setOrientation(LinearLayout.HORIZONTAL);

        ToggleButton tg1 = new ToggleButton(this);
        tg1.setText("1");
        tg1.setTextOn("x");
        tg1.setTextOff("1");

        ToggleButton tg2 = new ToggleButton(this);
        tg2.setText("2");
        tg2.setTextOn("x");
        tg2.setTextOff("2");

        ToggleButton tg3 = new ToggleButton(this);
        tg3.setText("3");
        tg3.setTextOn("x");
        tg3.setTextOff("3");

        ToggleButton tg4 = new ToggleButton(this);
        tg4.setText("4");
        tg4.setTextOn("x");
        tg4.setTextOff("4");

        ToggleButton tg5 = new ToggleButton(this);
        tg5.setText("5");
        tg5.setTextOn("x");
        tg5.setTextOff("5");

        ToggleButton tg6 = new ToggleButton(this);
        tg6.setText("6");
        tg6.setTextOn("x");
        tg6.setTextOff("6");

        layoutHorizontal.addView(tg1);
        layoutHorizontal.addView(tg2);
        layoutHorizontal.addView(tg3);
        layoutHorizontal.addView(tg4);
        layoutHorizontal.addView(tg5);
        layoutHorizontal.addView(tg6);

        layoutPrincipal.addView(layoutHorizontal);
        setContentView(layoutPrincipal);
*/
    }

    @NonNull
    private TableLayout.LayoutParams getLayoutParams() {
        return new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
    }
}